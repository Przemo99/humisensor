#!/usr/bin/env python
# -*- coding: utf_8 -*-
import serial
import time
import modbus_tk.defines as cst
import modbus_tk.modbus_rtu as modbus_rtu
import minimalmodbus as mbus

"""minimalmodbus settings"""
mbus.BYTESIZE = 8
mbus.STOPBITS = 1
mbus.TIMEOUT = 2
mbus.CLOSE_PORT_AFTER_EACH_CALL = True
mbus.BAUDRATE = 38400
mbus.PARITY = serial.PARITY_EVEN


def set_parameters(address, port, short_circuit):

    if short_circuit:
        stopbits = 1
        add = 247
    else:
        stopbits = 2
        add = 1

    error_flag = False

    try:
        # Connect to the slave
        master = modbus_rtu.RtuMaster(
            serial.Serial(port=port, baudrate=9600, bytesize=8, parity='N', stopbits=stopbits, xonxoff=0))
        master.set_timeout(5.0)
        master.set_verbose(True)

        master.execute(add, cst.WRITE_SINGLE_REGISTER, 4001, output_value=address)
        master.execute(add, cst.WRITE_SINGLE_REGISTER, 4002, output_value=3)
        master.execute(add, cst.WRITE_SINGLE_REGISTER, 4003, output_value=2)
        master.execute(add, cst.WRITE_SINGLE_REGISTER, 4004, output_value=1)
        print("Parametry wgrane! :)")
        master.close()

    except IOError:
        print("Nie mozna otworzyc portu")
        error_flag = True

    if not error_flag:
        print("Sprawdzam poprawnosc: ")

        for i in range(0, 4):
            time.sleep(1)
            try:
                connection = mbus.Instrument(port=port, slaveaddress=address)

                print(' ')
                print("Adres: " + str(connection.read_register(registeraddress=4001, functioncode=3)))
                print("Predkosc transmisji(3 -> 38400): " + str(connection.read_register(registeraddress=4002, functioncode=3)))
                print("Tryb transmisji(2 -> 8E1): " + str(connection.read_register(registeraddress=4003, functioncode=3)))
                print("Akceptacja zmian param (1): " + str(connection.read_register(registeraddress=4004, functioncode=3)))
                break
            except IOError:
                if i == 3:
                    print("Blad polaczenia master-slave.")
                pass
    else:
        print("Sprawdź zwore, parametry")

        # send some queries
        # logger.info(master.execute(1, cst.READ_COILS, 0, 10))
        # logger.info(master.execute(1, cst.READ_DISCRETE_INPUTS, 0, 8))
        # logger.info(master.execute(1, cst.READ_INPUT_REGISTERS, 100, 3))
        # logger.info(master.execute(1, cst.READ_HOLDING_REGISTERS, 100, 12))
        # logger.info(master.execute(1, cst.WRITE_SINGLE_COIL, 7, output_value=1))
        # logger.info(master.execute(1, cst.WRITE_SINGLE_REGISTER, 100, output_value=54))
        # logger.info(master.execute(1, cst.WRITE_MULTIPLE_COILS, 0, output_value=[1, 1, 0, 1, 1, 0, 1, 1]))
        # logger.info(master.execute(1, cst.WRITE_MULTIPLE_REGISTERS, 100, output_value=xrange(12)))
