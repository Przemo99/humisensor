#!/usr/bin/python2.4
# -*- coding: utf-8 -*-

from set import set_parameters
import sys


"""Wartości domyślne"""
COM_PORT = "COM4"
DEV_ADDRESS = 1


def save_params_to_file(params):
    header = "--- Plik konfiguracyjny ---\n"
    new_port = "COM_PORT=" + str(params[1]) + "\n"
    new_dev_add = "DEV_ADDRESS=" + str(params[0]) + "\n"

    try:
        with open("config.txt", 'w') as con:
            con.write(header)
            con.write(new_port)
            con.write(new_dev_add)
    except IOError:
        print("Input / Output Error - ladowanie wartosci domyslnych")


def load_params_from_file():
    try:
        with open("config.txt", 'r') as con:
            lines = con.readlines()
            params = []
            params.append(int(lines[2].split('=')[1]))
            params.append((lines[1].split('=')[1])[:-1])
            return params
    except IOError:
        print("Input / Output Error, plik nieistnieje lub jest zajety ladowanie wartosci domyslnych")
    except:
        print("Unknown ERROR")


def load_params_from_command_line(no_of_params):

    params = []
    # Dodanie pozniej - do poprawy TODO
    if sys.argv[1] == "Z" or sys.argv[1] == "z":
        if no_of_params == 3:
            params.append(int(sys.argv[2]))
            temp = load_params_from_file()
            params.append(temp[1])
        elif no_of_params == 4:
            params.append(int(sys.argv[2]))
            params.append("COM" + str(sys.argv[3]))
        elif no_of_params == 2:
            temp = load_params_from_file()
            params.append(temp[0])
            params.append(temp[1])
        params.append("Z")

    else:
        if no_of_params == 2:
            params.append(int(sys.argv[1]))
            temp = load_params_from_file()
            params.append(temp[1])
        elif no_of_params == 3:
            params.append(int(sys.argv[1]))
            params.append("COM" + str(sys.argv[2]))
    return params


if __name__ == "__main__":

    """
    Pobieranie kluczowych danych (SLAVE ADDRESS, PORT) kolejnosc:
     1. Linia komend main.py [ZWORA] [SLAVE_ADD] [PORT] (ustawienia automatycznie zapisuja sie w pliku config)
     2. Plik config
     3. Ustawienia domyslne

     params = [ZWORA (opcja),SLAVE_ADDRESS, PORT] {str, int, str} {Z,1, COM1}
    """

    # sys.argv.append("Z")
    # sys.argv.append(3)
    # sys.argv.append(10)

    if len(sys.argv) > 1:
        params = load_params_from_command_line(len(sys.argv))
        save_params_to_file(params)
    else:
        params = load_params_from_file()

    if not params:
        params = [DEV_ADDRESS, COM_PORT]
        save_params_to_file(params=params)

    """
    Ustawienie parametrow
    """
    if len(params) == 3:
        short_circuit = True
    else:
        short_circuit = False

    set_parameters(address=params[0], port=params[1], short_circuit=short_circuit)
