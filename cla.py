import argparse
import sys

parser = argparse.ArgumentParser(description="Program do konfiguracji parametrow protokolu Modbus RTU po interfejsie RS485 w czujniku wilgotnosci firmy LUMEL RH P18")

parser.add_argument("-p", 
                        dest = "port", 
                        metavar = "--port", 
                        default = "COM3", 
                        type = str, 
                        help="Port do ktorego podlaczony jest adapter USB->RS")
                        
parser.add_argument("-sa",
                        dest = "slave_address",
                        metavar = "--slave_address", 
                        default = 1, 
                        type = int, 
                        help="Adres modbus czujnika do przeprogramowania")

parser.add_argument("-sc",
                        dest = "short_circuit",
                        metavar = "--short_circuit", 
                        default = False, 
                        type = bool, 
                        help="Stan czujnika - zwarcie -> True (nadpisanie ustawien slave_address)")




# sys.argv.append("COM1")

args = parser.parse_args()

print(args.port)
print(args.slave_address)
print(args.short_circuit)

import os

def print_title():
    print("-------------------------------------------------")
    print("  Program do konfiguracji parametrow protokolu   ")
    print("   Modbus RTU po interfejsie RS485 w czujniku    ")
    print("       wilgotnosci firmy LUMEL RH P18            ")
    print("-------------------------------------------------")

    print("\n\n\n")


exit_list = ["exit", "e", "ex", "q", "quit"]
com_list = ["COM", "com", "Com", "cOm", "coM", "cmo", "CMO"]



while(True):
    print_title()
    cmd : str
    cmd = input("Insert command:        ")
    z = cmd.split("=")

    if z[0] in  exit_list : sys.exit()
    if z[0] in  com_list : print(z[1])

    # os.system("cls")
    print(z[0])
